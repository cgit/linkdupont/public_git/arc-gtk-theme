%global		debug_package	%{nil}
%global		gittag		20170302

Name:		arc-gtk-theme
Version:	1.0
Release:	%{gittag}.1%{?dist}
Summary:	A flat theme with transparent elements

License:	GPLv3
URL:		https://github.com/horst3180/arc-theme
Source0:	https://github.com/horst3180/arc-theme/archive/%{gittag}.tar.gz#/%{name}-%{gittag}.tar.gz

Requires:	gnome-themes-standard
Requires:	gtk-murrine-engine
BuildRequires:	gtk3-devel
BuildRequires:	autoconf
BuildRequires:	automake

BuildArch:	noarch


%description
%{summary}.


%prep
%autosetup -n arc-theme-%{gittag}

%build
NOCONFIGURE=1 ./autogen.sh
%configure
%make_build


%install
%make_install


%files
%license COPYING
%doc README.md HACKING.md AUTHORS
%{_datadir}/themes


%changelog
* Sat Jul 1 2017 Link Dupont <linkdupont@fedoraproject.org> - 1.0-20170302.1
- New upstream version

* Sat Nov 26 2016 Link Dupont <linkdupont@fedoraproject.org> - 1.0-20161119.1
- Initial package
